
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.htmlcleaner.HtmlCleaner;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomExample {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        File f = new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\RSS.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
        Element students = doc.getDocumentElement();
        NodeList studentList = students.getElementsByTagName("item");
        for (int i = 0; i < studentList.getLength(); i++) {
            Node node = studentList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element student = (Element) node;
                String sc = student.getElementsByTagName("description").item(0).getTextContent();
               
                System.out.println("img: " + removeHtmlFrom(sc));
            }
        }
    }
private static CharSequence removeHtmlFrom(String html) {
    return new HtmlCleaner().clean(html).getText();
}
    public static String extractUrls(String text) {
        String containedUrls = null;
        String urlRegex = "\\b((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        while (urlMatcher.find()) {
            String a = text.substring(urlMatcher.start(0), urlMatcher.end(0));
            //  System.out.println(""+a);
            if (!a.contains(".html")) {
                containedUrls = a;
            }
        }
        return containedUrls;
    }
}

//                Element student = (Element) node;
//
//                System.out.println("id: " + student.getAttribute("id"));
//                System.out.println("name: " + student.getElementsByTagName("name").item(0).getTextContent());
//                System.out.println("class: " + student.getElementsByTagName("class").item(0).getTextContent());
//                System.out.println("birthday: " + student.getElementsByTagName("birthday").item(0).getTextContent());
//
//                System.out.println("\n");
