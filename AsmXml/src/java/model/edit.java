/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.opensymphony.xwork2.ActionSupport;
import da.WriteCategory;
import entity.category;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author nam oio
 */
public class edit extends ActionSupport {
    String id;
    String name;
    String url;
    String id_delete;

    public String getId_delete() {
        return id_delete;
    }

    public void setId_delete(String id_delete) {
        this.id_delete = id_delete;
    }
  List<category> category = new LinkedList<category>();

    public List<category> getCategory() {
        return category;
    }

    public void setCategory(List<category> category) {
        this.category = category;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public edit() {
    }
    
    public String execute() throws Exception {
        if(id_delete!=null){
             new WriteCategory().delete(id_delete);
             
        return "admin";
        }
        if(name!=null&&url!=null){
        new WriteCategory().edit(id, name, url);
        return "admin";
    }
        category=new WriteCategory().filtedContent(id);
       return SUCCESS;
    }
    
}
