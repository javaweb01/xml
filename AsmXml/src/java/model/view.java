package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import entity.img;
import entity.video;
import entity.Post;
import com.opensymphony.xwork2.ActionSupport;

import java.util.LinkedList;
import java.util.List;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author nam oio
 */
public class view extends ActionSupport {

    public List<Post> post = new LinkedList<Post>();
    public List<img> image = new LinkedList<img>();
    public List<video> video = new LinkedList<video>();
    String title;
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<video> getVideo() {
        return video;
    }

    public void setVideo(List<video> video) {
        this.video = video;
    }

    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }

    public view() {
    }

    public String execute() throws Exception {
        html();

        return SUCCESS;
    }

    public void html() throws MalformedURLException, IOException {
        List<Post> p = new LinkedList<Post>();
        InputStream inStream = new URL(url).openStream();
        Document doc = Jsoup.parse(inStream, "UTF-8", url);
        final Elements tg = doc.select("p strong");
        String author = null;
        for (Element x : tg) {
            author = x.text();

        }
        final Elements ps = doc.select("p");
        System.out.println(ps.size());
        int count = 0;
        for (Element x : ps) {
            System.out.println("" + x.text());
            Post b = new Post();
            if (x.text().toString().contains("©")) {
                break;
            }
            b.setP(x.text().toString());
            post.add(b);
            if (x.text().toString().equals(author)) {
                break;
            }
        }
        final Elements ps1 = doc.select("tbody img");
        System.out.println(ps1.size());
        for (Element x : ps1) {
            img m = new img();
            m.setImg(x.attr("src"));
            image.add(m);
        }
        final Elements d = doc.select("video");
        for (Element x : d) {
            video vc = new video();
            vc.setVideo(x.attr("src"));
            video.add(vc);
        }
        final Elements tt = doc.select("h1");
        for (Element x : tt) {
            title = x.text();
        }
    }

    public List<img> getImage() {
        return image;
    }

    public void setImage(List<img> image) {
        this.image = image;
    }
}
