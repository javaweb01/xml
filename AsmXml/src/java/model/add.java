/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import da.WriteCategory;
import entity.category;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author nam oio
 */
public class add extends ActionSupport {
    
    public add() {
    }
    String id;
    String name;
    String url;
 
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
  
    public String execute() throws Exception {
       new WriteCategory().add(id, name, url);
       return SUCCESS;
    }
    
}
