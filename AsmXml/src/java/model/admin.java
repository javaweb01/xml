/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.opensymphony.xwork2.ActionSupport;
import entity.category;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import static model.ViewRss.viewcategory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author nam oio
 */
public class admin extends ActionSupport {
        List<category> category = new LinkedList<category>();

    public List<category> getCategory() {
        return category;
    }

    public void setCategory(List<category> category) {
        this.category = category;
    }
        
    public admin() {
    }
    
    public String execute() throws Exception {
        category=viewcategory();
       return SUCCESS;
    }
     public  List<category> viewcategory() throws ParserConfigurationException, SAXException, IOException {
        List<category> ct = new LinkedList<category>();
        File f = new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
        Element students = doc.getDocumentElement();
        NodeList studentList = students.getElementsByTagName("category");
        for (int i = 0; i < studentList.getLength(); i++) {
            Node node = studentList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element student = (Element) node;
                String id = student.getElementsByTagName("id").item(0).getTextContent();
                String name = student.getElementsByTagName("name").item(0).getTextContent();
                String url = student.getElementsByTagName("url").item(0).getTextContent();

                ct.add(new category(id, name, url));

            }
        }
        return ct;
    } 
}
