
import java.net.MalformedURLException;
import java.net.URL;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ReadRSS {

    public static void main(String[] args) throws SAXException, ParserConfigurationException, TransformerException {

        readRSSFeed("http://vietnamnet.vn/rss/home.rss");
    }

    public static void saveFile(String xmlSource) throws SAXException, ParserConfigurationException, IOException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            // Use String reader
            Document document = builder.parse(new InputSource(new StringReader(xmlSource)));
            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();
            Source src = new DOMSource(document);
            Result dest = new StreamResult(new File("src\\java\\file_xml\\RSS.xml"));
            aTransformer.transform(src, dest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readRSSFeed(String urlAddress) throws SAXException, ParserConfigurationException, TransformerException {
        try {
            URL rssUrl = new URL(urlAddress);
            BufferedReader in = new BufferedReader(new InputStreamReader(rssUrl.openStream()));
            String sourceCode = "";
            String line;
            String a = "";
            while ((line = in.readLine()) != null) {
                System.out.println(""+a);
                a = a + line;
            }
            in.close();
            saveFile(a);
            return sourceCode;
        } catch (MalformedURLException ue) {
            System.out.println("Malformed URL");
        } catch (IOException ioe) {
            System.out.println("Something went wrong reading the contents");
        }
        return null;
    }
}
