/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import entity.category;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nux.xom.xquery.XQueryUtil;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author nam oio
 */
public class WriteCategory1 {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException, XPathExpressionException {

        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = (Document) docBuilder.newDocument();
            Element rootElement = doc.createElement("company");
            doc.appendChild(rootElement);

            ArrayList<category> ct = new ArrayList<>();
            ct.add(new category("1", "Trang Chủ", "http://vietnamnet.vn/rss/home.rss"));
            ct.add(new category("2", "Pháp Luật", "	http://vietnamnet.vn/rss/phap-luat.rss"));
            ct.add(new category("3", "Công Nghệ", "http://vietnamnet.vn/rss/cong-nghe.rss"));
            ct.add(new category("4", "Kinh Doanh", "http://vietnamnet.vn/rss/kinh-doanh.rss"));
            ct.add(new category("5", "Giáo Dục", "http://vietnamnet.vn/rss/giao-duc.rss"));
            ct.add(new category("6", "Thời Sự", "	http://vietnamnet.vn/rss/thoi-su.rss"));
            ct.add(new category("7", "Giải Trí", "	http://vietnamnet.vn/rss/giai-tri.rss"));
            for (category o : ct) {

                // staff elements
                Element staff = doc.createElement("category");
                rootElement.appendChild(staff);

                Element id = doc.createElement("id");
                id.appendChild(doc.createTextNode(o.getId()));
                staff.appendChild(id);

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(o.getName()));
                staff.appendChild(name);

                Element st_class = doc.createElement("url");
                st_class.appendChild(doc.createTextNode(o.getUrl()));
                staff.appendChild(st_class);

            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));

            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
        view("1");
    }
public static void add() throws ParserConfigurationException, SAXException, IOException, TransformerException {
        File f = new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
     Element root = doc.getDocumentElement();
    NodeList rootlist = root.getChildNodes();
    for(int i=0; i<rootlist.getLength(); i++) {
        Element person = (Element)rootlist.item(i);
        NodeList personlist = person.getChildNodes();
        Element name = (Element)personlist.item(0);
        NodeList namelist = name.getChildNodes();
        Text nametext = (Text)namelist.item(0);
        String oldname = nametext.getData();
          
        if(oldname.equals("1")) {
            System.out.println(""+oldname);
            Element email = (Element)personlist.item(2);
            NodeList emaillist = email.getChildNodes();
            Text emailtext = (Text)emaillist.item(0);
            emailtext.setData("1r");
        }
   
}
    
    
   Transformer transformer = TransformerFactory.newInstance().newTransformer();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));

            transformer.transform(source, result);

            System.out.println("File saved!");
    }

public static void edit(String ed_id,String ed_name,String ed_url) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        File f = new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
     Element root = doc.getDocumentElement();
    NodeList rootlist = root.getChildNodes();
    for(int i=0; i<rootlist.getLength(); i++) {
        Element person = (Element)rootlist.item(i);
        NodeList personlist = person.getChildNodes();
        Element name = (Element)personlist.item(0);
        NodeList namelist = name.getChildNodes();
        Text nametext = (Text)namelist.item(0);
        String oldname = nametext.getData();    
        if(oldname.equals(ed_id)) {
            Element email = (Element)personlist.item(1);
            NodeList emaillist = email.getChildNodes();
            Text emailtext = (Text)emaillist.item(0);
            emailtext.setData(ed_name);
            
             Element email1 = (Element)personlist.item(2);
            NodeList emaillist1 = email.getChildNodes();
            Text emailtext1 = (Text)emaillist.item(0);
            emailtext1.setData(ed_url);
        }
   
    
    }
 Transformer transformer = TransformerFactory.newInstance().newTransformer();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));

            transformer.transform(source, result);

            System.out.println("File saved!");
}
 
  
    public static void view(String id) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        File f = new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
        Element students = doc.getDocumentElement();
        NodeList studentList = students.getElementsByTagName("category");
        for (int i = 0; i < studentList.getLength(); i++) {
            Node node = studentList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element student = (Element) node;
                String sc = student.getElementsByTagName("id").item(0).getTextContent();
if(sc.equals(id)){
     student.getParentNode().removeChild(student);
}
               
            }
        }
         Transformer transformer = TransformerFactory.newInstance().newTransformer();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));

            transformer.transform(source, result);

            System.out.println("File saved!");
    }
    
    
     public static List<category> filtedContent(String id) throws ParsingException, ValidityException, IOException {
      
         
         
         nu.xom.Document document = new nu.xom.Builder().build(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));
        nu.xom.Nodes nodes = XQueryUtil.xquery(document, "//category[id=" + id + "]");
         List<category> arrayList = new LinkedList<category>();
        for (int i = 0; i < nodes.size(); i++) {
            category pro = new category();
            nu.xom.Node node = nodes.get(i);
            pro.setId(node.getChild(0).getValue());
          pro.setName(node.getChild(1).getValue());
          pro.setUrl(node.getChild(2).getValue());
          
           
            arrayList.add(pro);
        }
      
        return arrayList;
    }
     
     public void changeContent(Document doc,String newname,String newemail) {
    Element root = doc.getDocumentElement();
    NodeList rootlist = root.getChildNodes();
    for(int i=0; i<rootlist.getLength(); i++) {
        Element person = (Element)rootlist.item(i);
        NodeList personlist = person.getChildNodes();
        Element name = (Element)personlist.item(0);
        NodeList namelist = name.getChildNodes();
        Text nametext = (Text)namelist.item(0);
        String oldname = nametext.getData();
        if(oldname.equals(newname)) {
            Element email = (Element)personlist.item(1);
            NodeList emaillist = email.getChildNodes();
            Text emailtext = (Text)emaillist.item(0);
            emailtext.setData(newemail);
        }
   
}
     }
     
     
     public static void delete(String id) throws XPathExpressionException, FileNotFoundException, TransformerConfigurationException, ParserConfigurationException, SAXException, IOException, TransformerException{

       File f = new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
     Element root = doc.getDocumentElement();
    NodeList rootlist = root.getChildNodes();
    for(int i=0; i<rootlist.getLength(); i++) {
        Element person = (Element)rootlist.item(i);
        NodeList personlist = person.getChildNodes();
        Element name = (Element)personlist.item(0);
        NodeList namelist = name.getChildNodes();
        Text nametext = (Text)namelist.item(0);
        String oldname = nametext.getData();    
        if(oldname.equals(id)) {
      
        
        }
  
}
     Transformer transformer = TransformerFactory.newInstance().newTransformer();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));

            transformer.transform(source, result);

            System.out.println("File saved!");
    }
     
     public static void clearChildNodes(Node node) throws ParserConfigurationException, SAXException, IOException{
     DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(true);
    factory.setExpandEntityReferences(false);

    Document doc = factory.newDocumentBuilder().parse(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\category.xml"));

    Element element = (Element) doc.getElementsByTagName("category").item(0);

    element.getParentNode().removeChild(element);

}
}
