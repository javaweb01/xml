package da;



import entity.rssPost;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.*;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.htmlcleaner.HtmlCleaner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class WriteFileRss {

 

    public static String saveFile(String xmlSource, String filename) throws SAXException, ParserConfigurationException, IOException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            // Use String reader
            Document document = builder.parse(new InputSource(new StringReader(xmlSource)));
            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();
            Source src = new DOMSource(document);
            Result dest = new StreamResult(new File("D:\\javaweb\\AsmXml\\src\\java\\file_xml\\" + filename + ".xml"));
            aTransformer.transform(src, dest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String f="D:\\javaweb\\AsmXml\\src\\java\\file_xml\\" + filename + ".xml";
        return f;
    }

    public static List<rssPost> wiriteRss(String url,String filename) throws SAXException, ParserConfigurationException, TransformerException {
          List<rssPost> post = new LinkedList<rssPost>();
        try {
            URL rssUrl = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(rssUrl.openStream(),Charset.forName("UTF-8")));
            
            String sourceCode = "";
            String line;
            String a = "";
            while ((line = in.readLine()) != null) {
                a = a + line;
            }
            in.close();
            System.out.println(""+a);
          post=  html(saveFile(a, "RSS"));
            return post;
        } catch (MalformedURLException ue) {
            System.out.println("Malformed URL");
        } catch (IOException ioe) {
            System.out.println("Something went wrong reading the contents");
        }
        return post;
    }

    public static List<rssPost> html(String file) throws MalformedURLException, IOException, SAXException, ParserConfigurationException {
       List<rssPost> post = new LinkedList<rssPost>();
           File f = new File(file);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
        Element students = doc.getDocumentElement();
        NodeList studentList = students.getElementsByTagName("item");
        for (int i = 0; i < studentList.getLength(); i++) {
            Node node = studentList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element student = (Element) node;
                String sc = student.getElementsByTagName("description").item(0).getTextContent();
                String title = student.getElementsByTagName("title").item(0).getTextContent();
                String pubDate = student.getElementsByTagName("pubDate").item(0).getTextContent();
              String link = student.getElementsByTagName("link").item(0).getTextContent();
              
                String img = extractUrls(sc);
                rssPost p = new rssPost();
                p.setImage(img);
                p.setTitle(title);
                p.setPubDate(pubDate);
                p.setUrl(link);
                p.setDescription(removeHtmlFrom(sc).toString());
                post.add(p);
            }
        }
        
        return post;
}
public static String extractUrls(String text) {
        String containedUrls = null;
        String urlRegex = "\\b((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        while (urlMatcher.find()) {
            String a = text.substring(urlMatcher.start(0), urlMatcher.end(0));
          
            if (!a.contains(".html")) {
                if(a.contains("?")){
                int count=a.indexOf("?");
                
                containedUrls = a.substring(0, count);
                }else{
                     containedUrls = a;
                }
            }
        }
        return containedUrls;
    }

   private static CharSequence removeHtmlFrom(String html) {
        return new HtmlCleaner().clean(html).getText();
    }
}
