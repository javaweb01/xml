package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class add_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_form_method_enctype_action;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_s_form_method_enctype_action = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_s_form_method_enctype_action.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\" ng-app=\"postApp\" ng-controller=\"postController\">\n");
      out.write("            <div class=\"col-sm-8 col-sm-offset-2\">\n");
      out.write("                <div class=\"page-header\"><h1>New Event</h1></div>\n");
      out.write("                <!-- FORM -->\n");
      out.write("                ");
      if (_jspx_meth_s_form_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("<style>\n");
      out.write("    body, html { font-family: sans-serif; height: 100%;}\n");
      out.write("\n");
      out.write("    .file-browse { margin-bottom: 5px; }\n");
      out.write("\n");
      out.write("    .button {\n");
      out.write("        background-color: cornflowerblue;\n");
      out.write("        color: white;\n");
      out.write("        float: left;\n");
      out.write("        font-weight: 100;\n");
      out.write("        line-height: 1;\n");
      out.write("        padding: 0.5em 1em;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .button-browse {\n");
      out.write("        position: relative;\n");
      out.write("        overflow: hidden;\n");
      out.write("    }\n");
      out.write("    .button-browse input[type=file] {\n");
      out.write("        position: absolute;\n");
      out.write("        top: 0;\n");
      out.write("        right: 0;\n");
      out.write("        min-width: 100%;\n");
      out.write("        min-height: 100%;\n");
      out.write("        font-size: 100px;\n");
      out.write("        text-align: right;\n");
      out.write("        filter: alpha(opacity=0);\n");
      out.write("        opacity: 0;\n");
      out.write("        outline: none;\n");
      out.write("        background: white;\n");
      out.write("        cursor: inherit;\n");
      out.write("        display: block;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .form1 {\n");
      out.write("        border: 1px solid lightgray;\n");
      out.write("        border-left: 0;\n");
      out.write("        font-family: sans-serif;\n");
      out.write("        font-size: .8em;\n");
      out.write("        line-height: 1.15;\n");
      out.write("        margin-left: 0;\n");
      out.write("        margin-top: 0;\n");
      out.write("        padding: .7em .6em .5em;\n");
      out.write("    }\n");
      out.write("    body {\n");
      out.write("        font-family: \"Helvetica Neue\",Helvetica,Arial,sans-serif;\n");
      out.write("        font-size: 14px;\n");
      out.write("        line-height: 1.42857143;\n");
      out.write("        color: #333;\n");
      out.write("        background-color: #fff;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .btn-primary {\n");
      out.write("        color: #fff;\n");
      out.write("        background-color: #337ab7;\n");
      out.write("        border-color: #2e6da4;\n");
      out.write("    }\n");
      out.write("    .btn {\n");
      out.write("        margin-top: 10px;\n");
      out.write("        display: inline-block;\n");
      out.write("        padding: 2px 12px;\n");
      out.write("        margin-bottom: 0;\n");
      out.write("        font-size: 14px;\n");
      out.write("        font-weight: 400;\n");
      out.write("        line-height: 1.42857143;\n");
      out.write("        text-align: center;\n");
      out.write("        white-space: nowrap;\n");
      out.write("        vertical-align: middle;\n");
      out.write("        -ms-touch-action: manipulation;\n");
      out.write("        touch-action: manipulation;\n");
      out.write("        cursor: pointer;\n");
      out.write("        -webkit-user-select: none;\n");
      out.write("        -moz-user-select: none;\n");
      out.write("        -ms-user-select: none;\n");
      out.write("        user-select: none;\n");
      out.write("        background-image: none;\n");
      out.write("        border: 1px solid transparent;\n");
      out.write("        border-radius: 4px;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("\n");
      out.write("    .form-control {\n");
      out.write("        margin-bottom: 10px;\n");
      out.write("        display: block;\n");
      out.write("        width: 70%;\n");
      out.write("        height: 34px;\n");
      out.write("        padding: 6px 12px;\n");
      out.write("        font-size: 14px;\n");
      out.write("        line-height: 1.42857143;\n");
      out.write("        color: #555;\n");
      out.write("        background-color: #fff;\n");
      out.write("        background-image: none;\n");
      out.write("        border: 1px solid #ccc;\n");
      out.write("        border-radius: 4px;\n");
      out.write("        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);\n");
      out.write("        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);\n");
      out.write("        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;\n");
      out.write("        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;\n");
      out.write("        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    .container {\n");
      out.write("        width: 750px;\n");
      out.write("    }\n");
      out.write("    .container {\n");
      out.write("        padding-right: 15px;\n");
      out.write("        padding-left: 15px;\n");
      out.write("        margin-right: auto;\n");
      out.write("        margin-left: 33%;\n");
      out.write("    }\n");
      out.write("    * {\n");
      out.write("        -webkit-box-sizing: border-box;\n");
      out.write("        -moz-box-sizing: border-box;\n");
      out.write("        box-sizing: border-box;\n");
      out.write("    }\n");
      out.write("</style>\n");
      out.write("<script src=\"jquery-3.3.1.js\" type=\"text/javascript\"></script>\n");
      out.write("<script src=\"jquery-3.3.1.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<script>\n");
      out.write("            $(document).on('change', '.button-browse :file', function () {\n");
      out.write("                var input = $(this),\n");
      out.write("                        numFiles = input.get(0).files ? input.get(0).files.length : 1,\n");
      out.write("                        label = input.val().replace(/\\\\/g, '/').replace(/.*\\//, '');\n");
      out.write("\n");
      out.write("                input.trigger('fileselect', [numFiles, label, input]);\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            $('.button-browse :file').on('fileselect', function (event, numFiles, label, input) {\n");
      out.write("                var val = numFiles > 1 ? numFiles + ' files selected' : label;\n");
      out.write("\n");
      out.write("                input.parent('.button-browse').next(':text').val(val);\n");
      out.write("            });\n");
      out.write("            var postApp = angular.module('postApp', []);\n");
      out.write("            postApp.controller('postController', function ($scope, $http) {\n");
      out.write("                $scope.user = {};\n");
      out.write("                $scope.submitForm = function () {\n");
      out.write("                    $http({\n");
      out.write("                        method: 'POST',\n");
      out.write("                        url: 'userImage',\n");
      out.write("                        data: $scope.user,\n");
      out.write("                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}\n");
      out.write("                    })\n");
      out.write("                            .success(function (data) {\n");
      out.write("                                if (data.errors) {\n");
      out.write("                                    $scope.errorName = data.errors.name;\n");
      out.write("                                    $scope.errorUserName = data.errors.username;\n");
      out.write("                                    $scope.errorEmail = data.errors.email;\n");
      out.write("                                } else {\n");
      out.write("                                    $scope.message = data.message;\n");
      out.write("                                }\n");
      out.write("                            });\n");
      out.write("                };\n");
      out.write("            });\n");
      out.write("</script>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_s_form_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:form
    org.apache.struts2.views.jsp.ui.FormTag _jspx_th_s_form_0 = (org.apache.struts2.views.jsp.ui.FormTag) _jspx_tagPool_s_form_method_enctype_action.get(org.apache.struts2.views.jsp.ui.FormTag.class);
    _jspx_th_s_form_0.setPageContext(_jspx_page_context);
    _jspx_th_s_form_0.setParent(null);
    _jspx_th_s_form_0.setAction("add");
    _jspx_th_s_form_0.setMethod("post");
    _jspx_th_s_form_0.setEnctype("multipart/form-data");
    int _jspx_eval_s_form_0 = _jspx_th_s_form_0.doStartTag();
    if (_jspx_eval_s_form_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_form_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_form_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_form_0.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                    <div class=\"form-group\">\n");
        out.write("                        <label>id</label>\n");
        out.write("                        <input type=\"text\" name=\"id\" class=\"form-control\" ng-model=\"user.name\">\n");
        out.write("\n");
        out.write("                    </div>\n");
        out.write("                    <div class=\"form-group\">\n");
        out.write("                        <label>name</label>\n");
        out.write("                        <input type=\"text\" name=\"name\" class=\"form-control\" ng-model=\"user.author\">\n");
        out.write("                    </div>\n");
        out.write("                     <div class=\"form-group\">\n");
        out.write("                        <label>url</label>\n");
        out.write("                        <input type=\"text\" name=\"url\" class=\"form-control\" ng-model=\"user.author\">\n");
        out.write("                    </div>\n");
        out.write("                    <button type=\"submit\" class=\"btn btn-primary\">Create Event</button>\n");
        out.write("                ");
        int evalDoAfterBody = _jspx_th_s_form_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_form_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_form_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_form_method_enctype_action.reuse(_jspx_th_s_form_0);
      return true;
    }
    _jspx_tagPool_s_form_method_enctype_action.reuse(_jspx_th_s_form_0);
    return false;
  }
}
