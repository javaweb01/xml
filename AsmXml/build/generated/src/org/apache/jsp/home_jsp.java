package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_iterator_var_value;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_iterator_value_status;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_iterator_value;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_property_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_if_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_s_iterator_var_value = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_iterator_value_status = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_iterator_value = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_property_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_s_iterator_var_value.release();
    _jspx_tagPool_s_iterator_value_status.release();
    _jspx_tagPool_s_iterator_value.release();
    _jspx_tagPool_s_property_value_nobody.release();
    _jspx_tagPool_s_if_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<nav>\n");
      out.write("  <div id=\"logo-menu\">\n");
      out.write("    <div class=\"content\">\n");
      out.write("      <ul>\n");
      out.write("        <li class=\"logo\">\n");
      out.write("          <a href=\"#\">\n");
      out.write("            <img src=\"https://static.bbci.co.uk/frameworks/barlesque/2.83.4/orb/4/img/bbc-blocks-dark.png\" alt=\"BBC\">\n");
      out.write("          </a>\n");
      out.write("        </li>\n");
      out.write("        <li class=\"sign-in\">\n");
      out.write("          <a class=\"underlined\" href=\"#\">\n");
      out.write("            <img src=\"https://static.bbci.co.uk/id/0.27.15/img/bbcid_orb_signin_dark.png\" alt=\"Profile\">\n");
      out.write("            <span>Sign in</span>\n");
      out.write("          </a>\n");
      out.write("        </li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">News</a></li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">Sport</a></li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">Weather</a></li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">iPlayer</a></li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">TV</a></li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">Radio</a></li>\n");
      out.write("        <li><a class=\"underlined\" href=\"#\">More</a></li>\n");
      out.write("        <li class=\"search\">\n");
      out.write("          <div class=\"search-container\">\n");
      out.write("            <input type=\"text\" placeholder=\"Search\">\n");
      out.write("            <a class=\"search-button\" href=\"#\">\n");
      out.write("              <img src=\"https://static.bbci.co.uk/frameworks/barlesque/2.83.4/orb/4/img/orb-search-dark.png\" alt=\"Search\">\n");
      out.write("            </a>\n");
      out.write("          </div>\n");
      out.write("        </li>\n");
      out.write("      </ul>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("\n");
      out.write("  <div class=\"red-header\">\n");
      out.write("    <div id=\"department\">\n");
      out.write("      <div class=\"content\">\n");
      out.write("        <a href=\"#\"><h1>News</h1></a>\n");
      out.write("        <div id=\"find-local\">\n");
      out.write("          <a href=\"#\">\n");
      out.write("            <p>Find local news</p>\n");
      out.write("          </a>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div id=\"primary-nav\">\n");
      out.write("      <div class=\"content\">\n");
      out.write("        <ul>\n");
      out.write("            ");
      if (_jspx_meth_s_iterator_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("\n");
      out.write("  \n");
      out.write("</nav>\n");
      out.write("\n");
      out.write("<main>\n");
      out.write("     ");
      if (_jspx_meth_s_iterator_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("  \n");
      out.write("</main>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<style>\n");
      out.write("    body {\n");
      out.write("  margin: 0;\n");
      out.write("  font-family: 'Arial', sans-serif;\n");
      out.write("}\n");
      out.write("a {\n");
      out.write("  color: black;\n");
      out.write("  text-decoration: none;\n");
      out.write("}\n");
      out.write("::-webkit-input-placeholder { color: black; }\n");
      out.write(":-moz-placeholder { color: black; }\n");
      out.write("::-moz-placeholder { color: black; }\n");
      out.write(":-ms-input-placeholder { color: black; }\n");
      out.write("\n");
      out.write(".content {\n");
      out.write("  max-width: 1000px;\n");
      out.write("  margin: 0 auto;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/*///////// NAV /////////*/\n");
      out.write("nav a { display: block; }\n");
      out.write("nav a.underlined {\n");
      out.write("  text-align: center;\n");
      out.write("  border-bottom: 4px solid transparent;\n");
      out.write("}\n");
      out.write("nav a.underlined:hover { border-bottom-color: #262626; }\n");
      out.write("nav #logo-menu li:nth-child(2) a.underlined:hover { border-bottom-color: #18b; }\n");
      out.write("nav #logo-menu li:nth-child(3) a.underlined:hover { border-bottom-color: #921; }\n");
      out.write("nav #logo-menu li:nth-child(4) a.underlined:hover { border-bottom-color: #fe1; }\n");
      out.write("nav #logo-menu li:nth-child(5) a.underlined:hover { border-bottom-color: #8ce; }\n");
      out.write("nav #logo-menu li:nth-child(6) a.underlined:hover { border-bottom-color: #ed3d7d; }\n");
      out.write("nav #primary-nav a.underlined:hover { border-bottom-color: white; }\n");
      out.write("nav #sub-nav a.underlined:hover { border-bottom-color: #cc0101; }\n");
      out.write("\n");
      out.write("nav #logo-menu {\n");
      out.write("  border-bottom: 1px solid #ccc;\n");
      out.write("}\n");
      out.write("nav #logo-menu .content {\n");
      out.write("  display: table;\n");
      out.write("  width: 100%;\n");
      out.write("}\n");
      out.write("nav #logo-menu ul {\n");
      out.write("  display: table-row;\n");
      out.write("  margin: 0;\n");
      out.write("  padding: 0;\n");
      out.write("  list-style-type: none;\n");
      out.write("}\n");
      out.write("nav #logo-menu li {\n");
      out.write("  display: table-cell;\n");
      out.write("  vertical-align: top;\n");
      out.write("  line-height: 40px;\n");
      out.write("  border-right: 1px solid #ccc;\n");
      out.write("  font-weight: bold;\n");
      out.write("  font-size: 14px;\n");
      out.write("}\n");
      out.write("nav #logo-menu .sign-in img {\n");
      out.write("  vertical-align: middle;\n");
      out.write("  margin-right: 5px;\n");
      out.write("}\n");
      out.write("nav #logo-menu .search {\n");
      out.write("  border-right: 0;\n");
      out.write("}\n");
      out.write("nav #logo-menu .logo { width: 1px; }\n");
      out.write("nav #logo-menu .search { width: 220px; }\n");
      out.write("nav #logo-menu .search .search-container {\n");
      out.write("  margin: 8px;\n");
      out.write("  height: 24px;\n");
      out.write("  background: #e4e4e4;\n");
      out.write("}\n");
      out.write("nav #logo-menu .search .search-container input,\n");
      out.write("nav #logo-menu .search .search-container img { vertical-align: top; }\n");
      out.write("nav #logo-menu .search .search-container input {\n");
      out.write("  background: transparent;\n");
      out.write("  border: none;\n");
      out.write("  width: 167px;\n");
      out.write("  padding: 0 0 0 5px;\n");
      out.write("  line-height: 24px;\n");
      out.write("  font-weight: bold;\n");
      out.write("  float: left;\n");
      out.write("}\n");
      out.write("nav #logo-menu .search .search-container .search-button {\n");
      out.write("  display: inline-block;\n");
      out.write("  height: 24px;\n");
      out.write("  width: 24px;\n");
      out.write("  text-align: center;\n");
      out.write("}\n");
      out.write("nav #logo-menu .search .search-container .search-button img {\n");
      out.write("  margin-top: 4px;\n");
      out.write("  vertical-align: top;\n");
      out.write("}\n");
      out.write("nav #logo-menu .logo img {\n");
      out.write("  display: inline-block;\n");
      out.write("  margin: 8px;\n");
      out.write("  vertical-align: top;\n");
      out.write("}\n");
      out.write("nav #logo-menu .sign-in { width: 170px; }\n");
      out.write("nav #logo-menu .sign-in a.underlined { text-align: left; }\n");
      out.write("nav #logo-menu li a.underlined {\n");
      out.write("  height: 36px;\n");
      out.write("  padding: 0 15px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("nav .red-header { background-color: #cc0101; }\n");
      out.write("\n");
      out.write("nav #department,\n");
      out.write("nav #primary-nav { color: white; }\n");
      out.write("\n");
      out.write("nav #department a {\n");
      out.write("  color: white;\n");
      out.write("  display: inline-block;\n");
      out.write("}\n");
      out.write("nav #department h1 {\n");
      out.write("  display: inline-block;\n");
      out.write("  margin: 0;\n");
      out.write("  padding: 3px 0;\n");
      out.write("  font-size: 2.8em;\n");
      out.write("  text-transform: uppercase;\n");
      out.write("  font-weight: normal;\n");
      out.write("}\n");
      out.write("nav #department #find-local {\n");
      out.write("  margin-top: 11px;\n");
      out.write("  padding: 0 25px 0 5px;\n");
      out.write("  float: right;\n");
      out.write("  border: 1px solid rgba(255,255,255,0.3);\n");
      out.write("  background: url(\"//static.bbci.co.uk/news/1.66.2287/img/news--icons-sprite.png\") no-repeat right -5902px;\n");
      out.write("}\n");
      out.write("nav #department #find-local a {\n");
      out.write("  margin: 0;\n");
      out.write("  padding: 4px 5px;\n");
      out.write("  font-size: 1.2em;\n");
      out.write("}\n");
      out.write("nav #department #find-local a:hover { text-decoration: underline; }\n");
      out.write("nav #department #find-local a p { margin: 0; }\n");
      out.write("\n");
      out.write("nav #primary-nav {\n");
      out.write("  border-top: 1px solid rgba(255,255,255,0.3);\n");
      out.write("}\n");
      out.write("nav #primary-nav .content {\n");
      out.write("  display: table;\n");
      out.write("  width: 100%;\n");
      out.write("}\n");
      out.write("nav #primary-nav ul {\n");
      out.write("  display: table-row;\n");
      out.write("  height: 36px;\n");
      out.write("}\n");
      out.write("nav #primary-nav li {\n");
      out.write("  display: table-cell;\n");
      out.write("  position: relative;\n");
      out.write("}\n");
      out.write("nav #primary-nav li:after {\n");
      out.write("  content: '';\n");
      out.write("  width: 1px;\n");
      out.write("  height: 18px;\n");
      out.write("  background-color: rgba(255,255,255,0.4);\n");
      out.write("  position: absolute;\n");
      out.write("  top: 8px;\n");
      out.write("  right: 0px;\n");
      out.write("}\n");
      out.write("nav #primary-nav li:last-child:after {\n");
      out.write("  content: none;\n");
      out.write("}\n");
      out.write("nav #primary-nav ul li:last-child a {\n");
      out.write("  padding-right: 20px;\n");
      out.write("}\n");
      out.write("nav #primary-nav ul li:last-child a:after {\n");
      out.write("  border-left: 4px solid transparent;\n");
      out.write("  border-right: 4px solid transparent;\n");
      out.write("  border-top: 4px solid #fff;\n");
      out.write("  content: '';\n");
      out.write("  height: 0;\n");
      out.write("  position: relative;\n");
      out.write("  right: -10px;\n");
      out.write("  top: 11px;\n");
      out.write("  width: 0;\n");
      out.write("}\n");
      out.write("nav #primary-nav ul li:last-child a:hover {\n");
      out.write("  border-bottom-color: white;\n");
      out.write("}\n");
      out.write("nav #primary-nav a {\n");
      out.write("  font-size: 15px;\n");
      out.write("  color: white;\n");
      out.write("  padding: 8px 12px;\n");
      out.write("  max-height: 16px;\n");
      out.write("}\n");
      out.write("nav #primary-nav ul li a.active { border-bottom-color: white; }\n");
      out.write("\n");
      out.write("nav #sub-nav ul {\n");
      out.write("  margin: 6px 0;\n");
      out.write("  padding: 0;\n");
      out.write("  list-style-type: none;\n");
      out.write("}\n");
      out.write("nav #sub-nav ul li {\n");
      out.write("  position: relative;\n");
      out.write("  display: inline-block;\n");
      out.write("}\n");
      out.write("nav #sub-nav ul li:after {\n");
      out.write("  content: '';\n");
      out.write("  width: 1px;\n");
      out.write("  height: 15px;\n");
      out.write("  background-color: rgba(0,0,0,0.15);\n");
      out.write("  position: absolute;\n");
      out.write("  top: 5px;\n");
      out.write("  right: 0px;\n");
      out.write("}\n");
      out.write("nav #sub-nav ul li:last-child:after { content: none; }\n");
      out.write("nav #sub-nav ul li a {\n");
      out.write("  font-size: 15px;\n");
      out.write("  padding: 3px 12px;\n");
      out.write("}\n");
      out.write("/*///////// End NAV /////////*/\n");
      out.write("\n");
      out.write(".tag {\n");
      out.write("  font-size: 80%;\n");
      out.write("  text-transform: uppercase;\n");
      out.write("  padding: 0px 4px;\n");
      out.write("  margin-right: 5px;\n");
      out.write("}\n");
      out.write(".tag.sport { background-color: #ffdf43; }\n");
      out.write("\n");
      out.write("main {\n");
      out.write("  margin-top: 20px;\n");
      out.write("}\n");
      out.write("main .content { position: relative; }\n");
      out.write("\n");
      out.write("main .main-content,\n");
      out.write("main .sidebar {\n");
      out.write("  box-sizing: border-box;\n");
      out.write("  display: inline-block;\n");
      out.write("  vertical-align: top;\n");
      out.write("}\n");
      out.write("\n");
      out.write("main .main-content {\n");
      out.write("  width: 70%;\n");
      out.write("  padding: 0 15px 0 6px;\n");
      out.write("}\n");
      out.write("main .main-content article {\n");
      out.write("  position: relative;\n");
      out.write("  min-height: 332px;\n");
      out.write("  padding: 0 0 30px 0;\n");
      out.write("  border-bottom: 1px solid #eee;\n");
      out.write("}\n");
      out.write("main .main-content article img.article-image {\n");
      out.write("  float: right;\n");
      out.write("  margin-left: 10px;\n");
      out.write("}\n");
      out.write("main .main-content article h2 {\n");
      out.write("  font-size: 2em;\n");
      out.write("  margin: 10px 0;\n");
      out.write("  color: black;\n");
      out.write("}\n");
      out.write("main .main-content article a:hover h2 { color: #1167a8; }\n");
      out.write("main .main-content article p {\n");
      out.write("  color: #5a5a5a;\n");
      out.write("  line-height: 1.2em;\n");
      out.write("  font-size: 0.9em;\n");
      out.write("}\n");
      out.write("article .post-data {\n");
      out.write("  display: inline-block;\n");
      out.write("  font-size: 0.8em;\n");
      out.write("  margin: 0 0 0 -10px;\n");
      out.write("  padding-left: 30px;\n");
      out.write("  background-image: url(\"//static.bbci.co.uk/news/1.66.2287/img/news--icons-sprite.png\");\n");
      out.write("  background-position: left -4011px;\n");
      out.write("  background-repeat: no-repeat;\n");
      out.write("}\n");
      out.write("article .post-data a.country {\n");
      out.write("  color: #cc0101;\n");
      out.write("  margin-left: 15px;\n");
      out.write("  position: relative;\n");
      out.write("}\n");
      out.write("main .main-content article .post-data a.country:hover { color: #1167a8; }\n");
      out.write("main .main-content article .post-data a.country:before {\n");
      out.write("  content: '';\n");
      out.write("  position: absolute;\n");
      out.write("  top: 1px;\n");
      out.write("  left: -8px;\n");
      out.write("  width: 1px;\n");
      out.write("  height: 15px;\n");
      out.write("  background: #ddd;\n");
      out.write("}\n");
      out.write("main .main-content article .related-links {\n");
      out.write("  position: relative;\n");
      out.write("  padding: 0;\n");
      out.write("  margin-top: 15px;\n");
      out.write("  list-style-type: none;\n");
      out.write("  font-size: 0.9em;\n");
      out.write("  line-height: 1.6em;\n");
      out.write("}\n");
      out.write("main .main-content article .related-links:before {\n");
      out.write("  content: '';\n");
      out.write("  position: absolute;\n");
      out.write("  top: -7px;\n");
      out.write("  left: 0;\n");
      out.write("  width: 40px;\n");
      out.write("  height: 1px;\n");
      out.write("  background: #cc0101;\n");
      out.write("}\n");
      out.write("main .main-content article .related-links a:hover { color: #1167a8; }\n");
      out.write("main .main-content article .related-links .icon {\n");
      out.write("  width: 16px;\n");
      out.write("  height: 16px;\n");
      out.write("  background: url(\"//myriad3d.co.uk/asset_hosting/codepen/bbc/sprite-2.png\") no-repeat;\n");
      out.write("  display: inline-block;\n");
      out.write("  vertical-align: middle;\n");
      out.write("  margin-right: 5px;\n");
      out.write("}\n");
      out.write("main .main-content article .related-links .icon.video { background-position: -8px -285px; }\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("main .sidebar {\n");
      out.write("  width: 26%;\n");
      out.write("  padding-left: 15px;\n");
      out.write("  border-left: 1px solid #eee;\n");
      out.write("}\n");
      out.write("main .sidebar h2 {\n");
      out.write("  margin-top: 0;\n");
      out.write("  font-weight: normal;\n");
      out.write("}\n");
      out.write("main .sidebar article { min-height: 60px; }\n");
      out.write("main .sidebar article h3 {font-size: 13px;\n");
      out.write("    width: 222px; }\n");
      out.write("main .sidebar article a:hover h3 { color: #1167a8; }\n");
      out.write("main .sidebar article .image {\n");
      out.write("  position: relative;\n");
      out.write("  margin-right: 10px;\n");
      out.write("  float: left;\n");
      out.write("}\n");
      out.write("main .sidebar article .image .time {\n");
      out.write("  position: absolute;\n");
      out.write("  bottom: 0;\n");
      out.write("  left: 0;\n");
      out.write("  padding: 6px 8px 6px 20px;\n");
      out.write("  background: white;\n");
      out.write("  font-size: 0.8em;\n");
      out.write("}\n");
      out.write("main .sidebar article .image .time:before {\n");
      out.write("  content: '';\n");
      out.write("  position: absolute;\n");
      out.write("  left: 5px;\n");
      out.write("  top: 7px;\n");
      out.write("  width: 0;\n");
      out.write("  height: 0;\n");
      out.write("  border-top: 6px solid transparent;\n");
      out.write("  border-bottom: 6px solid transparent;\n");
      out.write("  border-left: 10px solid black;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("<style>\n");
      out.write("    \n");
      out.write("        body p{\n");
      out.write("            font-weight: 400;\n");
      out.write("            font-family: arial;\n");
      out.write("            font-style: normal;\n");
      out.write("            font-size: 14px;\n");
      out.write("            line-height: 22px;\n");
      out.write("            color: #222222;\n");
      out.write("        }\n");
      out.write("        #c{\n");
      out.write("             margin-top: -110%;\n");
      out.write("    width: 68%;\n");
      out.write("    margin-left: 1%;\n");
      out.write("        }\n");
      out.write("    </style>\n");
      out.write("   \n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_s_iterator_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_0 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_value.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_0.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_0.setParent(null);
    _jspx_th_s_iterator_0.setValue("category");
    int _jspx_eval_s_iterator_0 = _jspx_th_s_iterator_0.doStartTag();
    if (_jspx_eval_s_iterator_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_0.doInitBody();
      }
      do {
        out.write("\n");
        out.write("          <li><a class=\"underlined\" href=\"viewrss?url=");
        if (_jspx_meth_s_property_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_0, _jspx_page_context))
          return true;
        out.write("\"><span>");
        if (_jspx_meth_s_property_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_0, _jspx_page_context))
          return true;
        out.write("</span></a></li>\n");
        out.write("          ");
        out.write("\n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_s_iterator_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_value.reuse(_jspx_th_s_iterator_0);
      return true;
    }
    _jspx_tagPool_s_iterator_value.reuse(_jspx_th_s_iterator_0);
    return false;
  }

  private boolean _jspx_meth_s_property_0(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_0 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_0.setPageContext(_jspx_page_context);
    _jspx_th_s_property_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_0);
    _jspx_th_s_property_0.setValue("url");
    int _jspx_eval_s_property_0 = _jspx_th_s_property_0.doStartTag();
    if (_jspx_th_s_property_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_0);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_0);
    return false;
  }

  private boolean _jspx_meth_s_property_1(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_1 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_1.setPageContext(_jspx_page_context);
    _jspx_th_s_property_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_0);
    _jspx_th_s_property_1.setValue("name");
    int _jspx_eval_s_property_1 = _jspx_th_s_property_1.doStartTag();
    if (_jspx_th_s_property_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_1);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_1);
    return false;
  }

  private boolean _jspx_meth_s_iterator_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_1 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_value_status.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_1.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_1.setParent(null);
    _jspx_th_s_iterator_1.setValue("post");
    _jspx_th_s_iterator_1.setStatus("stat");
    int _jspx_eval_s_iterator_1 = _jspx_th_s_iterator_1.doStartTag();
    if (_jspx_eval_s_iterator_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_1.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                ");
        if (_jspx_meth_s_iterator_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_1, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("               \n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_s_iterator_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_value_status.reuse(_jspx_th_s_iterator_1);
      return true;
    }
    _jspx_tagPool_s_iterator_value_status.reuse(_jspx_th_s_iterator_1);
    return false;
  }

  private boolean _jspx_meth_s_iterator_2(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_2 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_var_value.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_2.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_1);
    _jspx_th_s_iterator_2.setVar("ind");
    _jspx_th_s_iterator_2.setValue("1");
    int _jspx_eval_s_iterator_2 = _jspx_th_s_iterator_2.doStartTag();
    if (_jspx_eval_s_iterator_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_2.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_2.doInitBody();
      }
      do {
        out.write("\n");
        out.write("    ");
        if (_jspx_meth_s_if_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_2, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("  ");
        int evalDoAfterBody = _jspx_th_s_iterator_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_var_value.reuse(_jspx_th_s_iterator_2);
      return true;
    }
    _jspx_tagPool_s_iterator_var_value.reuse(_jspx_th_s_iterator_2);
    return false;
  }

  private boolean _jspx_meth_s_if_0(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:if
    org.apache.struts2.views.jsp.IfTag _jspx_th_s_if_0 = (org.apache.struts2.views.jsp.IfTag) _jspx_tagPool_s_if_test.get(org.apache.struts2.views.jsp.IfTag.class);
    _jspx_th_s_if_0.setPageContext(_jspx_page_context);
    _jspx_th_s_if_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_2);
    _jspx_th_s_if_0.setTest("#stat.count == #ind");
    int _jspx_eval_s_if_0 = _jspx_th_s_if_0.doStartTag();
    if (_jspx_eval_s_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_if_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_if_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_if_0.doInitBody();
      }
      do {
        out.write("\n");
        out.write("     \n");
        out.write("    \n");
        out.write("    \n");
        out.write("      <div class=\"content\">\n");
        out.write("    <div class=\"main-content\">\n");
        out.write("      <article class=\"full-width\">\n");
        out.write("        <a href=\"#\">\n");
        out.write("          <h2><a href=\"view?url=");
        if (_jspx_meth_s_property_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write('"');
        out.write('>');
        if (_jspx_meth_s_property_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write("</h2>\n");
        out.write("          <img style=\"width: 450px;\"  class=\"article-image\" src=\"");
        if (_jspx_meth_s_property_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write("\" alt=\"Article image\">\n");
        out.write("          <p>");
        if (_jspx_meth_s_property_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write("</p>\n");
        out.write("          <p>");
        if (_jspx_meth_s_property_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write("</p>\n");
        out.write("        </a>\n");
        out.write("      \n");
        out.write("     \n");
        out.write("      </article>\n");
        out.write("    </div><!--\n");
        out.write("    -->  \n");
        out.write("    <div class=\"sidebar\">\n");
        out.write("      <h2>Watch/Listen</h2>\n");
        out.write("     ");
        if (_jspx_meth_s_iterator_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("      </div>\n");
        out.write("    \n");
        out.write("    <br/>\n");
        out.write("        <div id=\"c\">\n");
        out.write("           \n");
        out.write("            \n");
        out.write("            \n");
        out.write("            \n");
        out.write("            \n");
        out.write("            \n");
        out.write("            \n");
        out.write("            \n");
        out.write("            ");
        if (_jspx_meth_s_iterator_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("\n");
        out.write("        </div>\n");
        out.write("  </div>\n");
        out.write("    \n");
        out.write("    \n");
        out.write("    \n");
        out.write("    \n");
        out.write("    \n");
        out.write("    \n");
        out.write("    ");
        int evalDoAfterBody = _jspx_th_s_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_if_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_0);
      return true;
    }
    _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_0);
    return false;
  }

  private boolean _jspx_meth_s_property_2(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_2 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_2.setPageContext(_jspx_page_context);
    _jspx_th_s_property_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_property_2.setValue("url");
    int _jspx_eval_s_property_2 = _jspx_th_s_property_2.doStartTag();
    if (_jspx_th_s_property_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_2);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_2);
    return false;
  }

  private boolean _jspx_meth_s_property_3(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_3 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_3.setPageContext(_jspx_page_context);
    _jspx_th_s_property_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_property_3.setValue("title");
    int _jspx_eval_s_property_3 = _jspx_th_s_property_3.doStartTag();
    if (_jspx_th_s_property_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_3);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_3);
    return false;
  }

  private boolean _jspx_meth_s_property_4(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_4 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_4.setPageContext(_jspx_page_context);
    _jspx_th_s_property_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_property_4.setValue("image");
    int _jspx_eval_s_property_4 = _jspx_th_s_property_4.doStartTag();
    if (_jspx_th_s_property_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_4);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_4);
    return false;
  }

  private boolean _jspx_meth_s_property_5(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_5 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_5.setPageContext(_jspx_page_context);
    _jspx_th_s_property_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_property_5.setValue("description");
    int _jspx_eval_s_property_5 = _jspx_th_s_property_5.doStartTag();
    if (_jspx_th_s_property_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_5);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_5);
    return false;
  }

  private boolean _jspx_meth_s_property_6(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_6 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_6.setPageContext(_jspx_page_context);
    _jspx_th_s_property_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_property_6.setValue("pubDate");
    int _jspx_eval_s_property_6 = _jspx_th_s_property_6.doStartTag();
    if (_jspx_th_s_property_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_6);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_6);
    return false;
  }

  private boolean _jspx_meth_s_iterator_3(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_3 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_value_status.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_3.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_iterator_3.setValue("post");
    _jspx_th_s_iterator_3.setStatus("stat");
    int _jspx_eval_s_iterator_3 = _jspx_th_s_iterator_3.doStartTag();
    if (_jspx_eval_s_iterator_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_3 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_3.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_3.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                ");
        if (_jspx_meth_s_iterator_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_3, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("               \n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_s_iterator_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_3 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_value_status.reuse(_jspx_th_s_iterator_3);
      return true;
    }
    _jspx_tagPool_s_iterator_value_status.reuse(_jspx_th_s_iterator_3);
    return false;
  }

  private boolean _jspx_meth_s_iterator_4(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_4 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_var_value.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_4.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_3);
    _jspx_th_s_iterator_4.setVar("ind");
    _jspx_th_s_iterator_4.setValue("1");
    int _jspx_eval_s_iterator_4 = _jspx_th_s_iterator_4.doStartTag();
    if (_jspx_eval_s_iterator_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_4 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_4.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_4.doInitBody();
      }
      do {
        out.write("\n");
        out.write("    ");
        if (_jspx_meth_s_if_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_4, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("  ");
        int evalDoAfterBody = _jspx_th_s_iterator_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_4 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_var_value.reuse(_jspx_th_s_iterator_4);
      return true;
    }
    _jspx_tagPool_s_iterator_var_value.reuse(_jspx_th_s_iterator_4);
    return false;
  }

  private boolean _jspx_meth_s_if_1(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:if
    org.apache.struts2.views.jsp.IfTag _jspx_th_s_if_1 = (org.apache.struts2.views.jsp.IfTag) _jspx_tagPool_s_if_test.get(org.apache.struts2.views.jsp.IfTag.class);
    _jspx_th_s_if_1.setPageContext(_jspx_page_context);
    _jspx_th_s_if_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_4);
    _jspx_th_s_if_1.setTest("1 <#stat.count && #stat.count < 9");
    int _jspx_eval_s_if_1 = _jspx_th_s_if_1.doStartTag();
    if (_jspx_eval_s_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_if_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_if_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_if_1.doInitBody();
      }
      do {
        out.write("\n");
        out.write("    \n");
        out.write("   \n");
        out.write("      <article>\n");
        out.write("        <a href=\"#\">\n");
        out.write("          <div class=\"image\">\n");
        out.write("              <img style=\"width: 220px;\" src=\"");
        if (_jspx_meth_s_property_7((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_1, _jspx_page_context))
          return true;
        out.write("\" alt=\"Article image\">\n");
        out.write("           \n");
        out.write("          </div>\n");
        out.write("          <h3>");
        if (_jspx_meth_s_property_8((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_1, _jspx_page_context))
          return true;
        out.write("</h3>\n");
        out.write("          <p class=\"post-data\">");
        if (_jspx_meth_s_property_9((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_1, _jspx_page_context))
          return true;
        out.write("</p>\n");
        out.write("        </a>\n");
        out.write("      </article>\n");
        out.write("     \n");
        out.write("    \n");
        out.write("    \n");
        out.write("    \n");
        out.write("    ");
        int evalDoAfterBody = _jspx_th_s_if_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_if_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_1);
      return true;
    }
    _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_1);
    return false;
  }

  private boolean _jspx_meth_s_property_7(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_7 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_7.setPageContext(_jspx_page_context);
    _jspx_th_s_property_7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_1);
    _jspx_th_s_property_7.setValue("image");
    int _jspx_eval_s_property_7 = _jspx_th_s_property_7.doStartTag();
    if (_jspx_th_s_property_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_7);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_7);
    return false;
  }

  private boolean _jspx_meth_s_property_8(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_8 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_8.setPageContext(_jspx_page_context);
    _jspx_th_s_property_8.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_1);
    _jspx_th_s_property_8.setValue("title");
    int _jspx_eval_s_property_8 = _jspx_th_s_property_8.doStartTag();
    if (_jspx_th_s_property_8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_8);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_8);
    return false;
  }

  private boolean _jspx_meth_s_property_9(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_9 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_9.setPageContext(_jspx_page_context);
    _jspx_th_s_property_9.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_1);
    _jspx_th_s_property_9.setValue("pubDate");
    int _jspx_eval_s_property_9 = _jspx_th_s_property_9.doStartTag();
    if (_jspx_th_s_property_9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_9);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_9);
    return false;
  }

  private boolean _jspx_meth_s_iterator_5(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_5 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_value_status.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_5.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_0);
    _jspx_th_s_iterator_5.setValue("post");
    _jspx_th_s_iterator_5.setStatus("stat");
    int _jspx_eval_s_iterator_5 = _jspx_th_s_iterator_5.doStartTag();
    if (_jspx_eval_s_iterator_5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_5 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_5.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_5.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                ");
        if (_jspx_meth_s_iterator_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_5, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("               \n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_s_iterator_5.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_5 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_value_status.reuse(_jspx_th_s_iterator_5);
      return true;
    }
    _jspx_tagPool_s_iterator_value_status.reuse(_jspx_th_s_iterator_5);
    return false;
  }

  private boolean _jspx_meth_s_iterator_6(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_5, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_6 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_var_value.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_6.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_5);
    _jspx_th_s_iterator_6.setVar("ind");
    _jspx_th_s_iterator_6.setValue("9");
    int _jspx_eval_s_iterator_6 = _jspx_th_s_iterator_6.doStartTag();
    if (_jspx_eval_s_iterator_6 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_6 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_6.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_6.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                    ");
        if (_jspx_meth_s_if_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_6, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("  ");
        int evalDoAfterBody = _jspx_th_s_iterator_6.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_6 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_var_value.reuse(_jspx_th_s_iterator_6);
      return true;
    }
    _jspx_tagPool_s_iterator_var_value.reuse(_jspx_th_s_iterator_6);
    return false;
  }

  private boolean _jspx_meth_s_if_2(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_6, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:if
    org.apache.struts2.views.jsp.IfTag _jspx_th_s_if_2 = (org.apache.struts2.views.jsp.IfTag) _jspx_tagPool_s_if_test.get(org.apache.struts2.views.jsp.IfTag.class);
    _jspx_th_s_if_2.setPageContext(_jspx_page_context);
    _jspx_th_s_if_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_6);
    _jspx_th_s_if_2.setTest("#stat.count > #ind");
    int _jspx_eval_s_if_2 = _jspx_th_s_if_2.doStartTag();
    if (_jspx_eval_s_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_if_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_if_2.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_if_2.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                        \n");
        out.write("                        \n");
        out.write("                        <div >     \n");
        out.write("                         <div style=\"height: 32px;\"></div>\n");
        out.write("                        <img src=\"");
        if (_jspx_meth_s_property_10((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_2, _jspx_page_context))
          return true;
        out.write("\" style=\"\n");
        out.write("             width: 246px;\n");
        out.write("             height:  144px;\n");
        out.write("             float: left;\">\n");
        out.write("        <div style=\"    width: 422px;\n");
        out.write("    padding-top: 0px;\n");
        out.write("    padding-left: 257px;\">\n");
        out.write("\n");
        out.write("            <h3 style=\"margin-top: 0px; font-family: Helmet, Freesans, Helvetica, Arial, sans-serif;\"> <span><a style=\"    text-decoration: none;\" href=\"view?url=");
        if (_jspx_meth_s_property_11((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_2, _jspx_page_context))
          return true;
        out.write('"');
        out.write('>');
        if (_jspx_meth_s_property_12((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_2, _jspx_page_context))
          return true;
        out.write("</a></span></h3>\n");
        out.write("            <p style=\"    font-family: Helmet, Freesans, Helvetica, Arial, sans-serif;\">");
        if (_jspx_meth_s_property_13((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_2, _jspx_page_context))
          return true;
        out.write("</p> \n");
        out.write("            ");
        if (_jspx_meth_s_property_14((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_if_2, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("        </div>\n");
        out.write("                        \n");
        out.write("        </div>            \n");
        out.write("       \n");
        out.write("    ");
        int evalDoAfterBody = _jspx_th_s_if_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_if_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_2);
      return true;
    }
    _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_2);
    return false;
  }

  private boolean _jspx_meth_s_property_10(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_10 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_10.setPageContext(_jspx_page_context);
    _jspx_th_s_property_10.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_2);
    _jspx_th_s_property_10.setValue("image");
    int _jspx_eval_s_property_10 = _jspx_th_s_property_10.doStartTag();
    if (_jspx_th_s_property_10.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_10);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_10);
    return false;
  }

  private boolean _jspx_meth_s_property_11(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_11 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_11.setPageContext(_jspx_page_context);
    _jspx_th_s_property_11.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_2);
    _jspx_th_s_property_11.setValue("url");
    int _jspx_eval_s_property_11 = _jspx_th_s_property_11.doStartTag();
    if (_jspx_th_s_property_11.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_11);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_11);
    return false;
  }

  private boolean _jspx_meth_s_property_12(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_12 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_12.setPageContext(_jspx_page_context);
    _jspx_th_s_property_12.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_2);
    _jspx_th_s_property_12.setValue("title");
    int _jspx_eval_s_property_12 = _jspx_th_s_property_12.doStartTag();
    if (_jspx_th_s_property_12.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_12);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_12);
    return false;
  }

  private boolean _jspx_meth_s_property_13(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_13 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_13.setPageContext(_jspx_page_context);
    _jspx_th_s_property_13.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_2);
    _jspx_th_s_property_13.setValue("description");
    int _jspx_eval_s_property_13 = _jspx_th_s_property_13.doStartTag();
    if (_jspx_th_s_property_13.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_13);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_13);
    return false;
  }

  private boolean _jspx_meth_s_property_14(javax.servlet.jsp.tagext.JspTag _jspx_th_s_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_14 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_14.setPageContext(_jspx_page_context);
    _jspx_th_s_property_14.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_if_2);
    _jspx_th_s_property_14.setValue("pubDate");
    int _jspx_eval_s_property_14 = _jspx_th_s_property_14.doStartTag();
    if (_jspx_th_s_property_14.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_14);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_14);
    return false;
  }
}
