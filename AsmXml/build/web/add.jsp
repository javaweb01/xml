<%-- 
    Document   : PostBook
    Created on : May 15, 2018, 12:00:44 AM
    Author     : nam oio
--%>

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container" ng-app="postApp" ng-controller="postController">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="page-header"><h1>Create Item</h1></div>
                <!-- FORM -->
                <s:form action="add" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>id</label>
                        <input type="text" name="id" class="form-control" ng-model="user.name">

                    </div>
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" name="name" class="form-control" ng-model="user.author">
                    </div>
                     <div class="form-group">
                        <label>url</label>
                        <input type="text" name="url" class="form-control" ng-model="user.author">
                    </div>
                    <button type="submit" class="btn btn-primary">Add item</button>
                </s:form>
            </div>
        </div>
    </body>
</html>
<style>
    body, html { font-family: sans-serif; height: 100%;}

    .file-browse { margin-bottom: 5px; }

    .button {
        background-color: cornflowerblue;
        color: white;
        float: left;
        font-weight: 100;
        line-height: 1;
        padding: 0.5em 1em;
    }

    .button-browse {
        position: relative;
        overflow: hidden;
    }
    .button-browse input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    .form1 {
        border: 1px solid lightgray;
        border-left: 0;
        font-family: sans-serif;
        font-size: .8em;
        line-height: 1.15;
        margin-left: 0;
        margin-top: 0;
        padding: .7em .6em .5em;
    }
    body {
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857143;
        color: #333;
        background-color: #fff;
    }

    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn {
        margin-top: 10px;
        display: inline-block;
        padding: 2px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }


    .form-control {
        margin-bottom: 10px;
        display: block;
        width: 70%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }

    .container {
        width: 750px;
    }
    .container {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: 33%;
    }
    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
</style>
<script src="jquery-3.3.1.js" type="text/javascript"></script>
<script src="jquery-3.3.1.min.js" type="text/javascript"></script>
<script>
            $(document).on('change', '.button-browse :file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

                input.trigger('fileselect', [numFiles, label, input]);
            });

            $('.button-browse :file').on('fileselect', function (event, numFiles, label, input) {
                var val = numFiles > 1 ? numFiles + ' files selected' : label;

                input.parent('.button-browse').next(':text').val(val);
            });
            var postApp = angular.module('postApp', []);
            postApp.controller('postController', function ($scope, $http) {
                $scope.user = {};
                $scope.submitForm = function () {
                    $http({
                        method: 'POST',
                        url: 'userImage',
                        data: $scope.user,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    })
                            .success(function (data) {
                                if (data.errors) {
                                    $scope.errorName = data.errors.name;
                                    $scope.errorUserName = data.errors.username;
                                    $scope.errorEmail = data.errors.email;
                                } else {
                                    $scope.message = data.message;
                                }
                            });
                };
            });
</script>
