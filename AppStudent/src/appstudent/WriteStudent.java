/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appstudent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nux.xom.xquery.XQueryUtil;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author nam oio
 */
public class WriteStudent {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            // root elements
            Document doc = (Document) docBuilder.newDocument();
            Element rootElement = doc.createElement("student");
            doc.appendChild(rootElement);
            ArrayList<Student> ct = new ArrayList<>();
            ct.add(new Student(1, "Phan huy Duong", "Hai Phong"));
            ct.add(new Student(2, "Nguyen Thanh Long", "Hong Kong"));
            for (Student st : ct) {
                // staff elements
                Element staff = doc.createElement("student");
                rootElement.appendChild(staff);
                Element id = doc.createElement("id");
                id.appendChild(doc.createTextNode(st.getId() + ""));
                staff.appendChild(id);
                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(st.getName()));
                staff.appendChild(name);
                Element st_class = doc.createElement("address");
                st_class.appendChild(doc.createTextNode(st.getAddress()));
                staff.appendChild(st_class);

            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\javaweb\\AppStudent\\src\\fileXML\\student.xml"));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);
            System.out.println("File saved!");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

}
