/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appstudent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nux.xom.xquery.XQueryUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author nam oio
 */
public class ListStudent {

    private static String FileName = "D:\\javaweb\\AppStudent\\src\\fileXML\\student.xml";

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, ParsingException {
        Scanner sc = new Scanner(System.in);
        Scanner sc1 = new Scanner(System.in);
        String yn = "Y";
        while (yn.equals("Y")) {
            System.out.println("1: Search ");
            System.out.println("2: View Student ");
            int choice;
            choice = Integer.parseInt(sc.nextLine());
            menu(choice);
            System.out.println("Tiếp Tục (Y/N): ");
            String c = sc1.nextLine();
            if (c.equals("Y")) {
                yn = "Y";
            } else {
                yn = "N";
            }
        }
    }

    public static void menu(int choice) throws ParserConfigurationException, SAXException, IOException, ParsingException {
        switch (choice) {
            case 1:
                System.out.println("Search id: ");
                Scanner sc = new Scanner(System.in);
                int id = Integer.parseInt(sc.nextLine());
                  ArrayList<Student> arrayList = new ArrayList<>();
                search(id);
                break;
            case 2:
                view();
                break;
            default:
                System.out.println("error");
        }
    }

    public static ArrayList<Student> search(int id) throws ParsingException, ValidityException, IOException {
        nu.xom.Document document = new nu.xom.Builder().build(new File(FileName));
        nu.xom.Nodes nodes = XQueryUtil.xquery(document, "//student[id=" + id + "]");
        ArrayList<Student> arrayList = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            Student pro = new Student();
            nu.xom.Node node = nodes.get(i);
            pro.setId(Integer.parseInt(node.getChild(1).getValue()));
            pro.setName(node.getChild(3).getValue());
            pro.setAddress(node.getChild(5).getValue());
            arrayList.add(pro);
            System.out.println(pro.toString());
        }
         if(arrayList.size()==0){
            System.out.println("id không có");
        }
        return arrayList;
    }

    public static ArrayList<Student> view() throws ParserConfigurationException, SAXException, IOException {
        File f = new File(FileName);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
        Element students = doc.getDocumentElement();
        NodeList studentList = students.getElementsByTagName("student");
        ArrayList<Student> st = new ArrayList<>();
        for (int i = 0; i < studentList.getLength(); i++) {
            Node node = studentList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element student = (Element) node;
                int id = Integer.parseInt(student.getElementsByTagName("id").item(0).getTextContent());
                String name = student.getElementsByTagName("name").item(0).getTextContent();
                String address = student.getElementsByTagName("address").item(0).getTextContent();
                Student c = new Student();
                c.setId(id);
                c.setName(name);
                c.setAddress(address);
                st.add(c);
                System.out.println(c.toString());
            }
        }
       
        return st;
    }
}
