/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.transform.Transform;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author nam oio
 */
public class DAO {

    protected Connection connection;

    public DAO() {

        try {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            } catch (InstantiationException ex) {
                Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        String url = "jdbc:sqlserver://localhost:1433;databaseName=Products";
        String username = "sa";
        String password = "123456";
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ResultSet getData() {
        ResultSet rs = null;
        try {
            Statement stmt = connection.createStatement();

            rs = stmt.executeQuery("select * from product");
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public void writeXML(String url) throws ParserConfigurationException, SQLException, TransformerConfigurationException, TransformerException, IOException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = (Document) builder.newDocument();
        Element root = document.createElement("Products");
        document.appendChild(root);
        ResultSet rs = this.getData();
        while (rs.next()) {
            boolean a=true;
            Element child = document.createElement("Product");
            root.appendChild(child);

            Element ProductID = document.createElement("ProductID");
            ProductID.setTextContent((rs.getString("ProductID")));
            child.appendChild(ProductID);

            Element ProductName = document.createElement("ProductName");
            ProductName.setTextContent((rs.getString("ProductName")));
            child.appendChild(ProductName);

            Element SupplierID = document.createElement("SupplierID");
            SupplierID.setTextContent((rs.getString("SupplierID")));
            child.appendChild(SupplierID);

            Element CategoryID = document.createElement("CategoryID");
            CategoryID.setTextContent((rs.getString("CategoryID")));
            child.appendChild(CategoryID);

            Element QuantityPerUnit = document.createElement("QuantityPerUnit");
            QuantityPerUnit.setTextContent(("1"));
            child.appendChild(QuantityPerUnit);

            Element UnitsPrice = document.createElement("UnitsPrice");
            UnitsPrice.setTextContent((rs.getString("UnitsPrice")));
            child.appendChild(UnitsPrice);

            Element UnitsInStock = document.createElement("UnitsInStock");
            UnitsInStock.setTextContent((rs.getString("UnitsInStock")));
            child.appendChild(UnitsInStock);

            Element UnitsOnOrder = document.createElement("UnitsOnOrder");
            UnitsOnOrder.setTextContent((rs.getString("UnitsOnOrder")));
            child.appendChild(UnitsOnOrder);

            Element ReorderLevel = document.createElement("ReorderLevel");
            ReorderLevel.setTextContent((rs.getString("ReorderLevel")));
            child.appendChild(ReorderLevel);

            Element Discontinued = document.createElement("Discontinued");
            Discontinued.setTextContent((rs.getString("Discontinued")));
            child.appendChild(Discontinued);
        }
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "iso-8859-1");
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(document);
        transformer.transform(source, result);
        FileWriter fwriter = new FileWriter(new File(url + "Products.xml"));
        fwriter.write(writer.toString());
        fwriter.close();

    }
}
