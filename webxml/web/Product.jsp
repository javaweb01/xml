<%-- 
    Document   : Product
    Created on : May 24, 2018, 12:55:38 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml-stylesheet type="text/xsl" href="product.xsl"?>
<!DOCTYPE html>
<Products>
    <c:forEach  var="product" items="${products}">
        <Product>
        <ProductName>${product.ProductName}</ProductName>
        </Product>
    </c:forEach>
    
</Products>
