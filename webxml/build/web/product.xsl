<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : product.xsl
    Created on : May 24, 2018, 2:20 PM
    Author     : nam oio
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <table border="1">
                <tr bgcolor="#9acd32">
                    <th>ProductID</th>
                     <th>ProductName</th>
                      <th>SupplierID</th>
                       <th>CategoryID</th>
                        <th>QuantityPerUnit</th>
                         <th>UnitsPrice</th>
                          <th>UnitsInStock</th>
                           <th>UnitsOnOrder</th>
                            <th>ReorderLevel</th>
                             <th>Discontinued</th>
                </tr>
                <xsl:for-each select="Products/Product">
                    <td>
                        <xsl:value-of select="ProductID"/>
                    </td>
                     <td>
                        <xsl:value-of select="ProductName"/>
                    </td>
                     <td>
                        <xsl:value-of select="SupplierID"/>
                    </td>
                     <td>
                        <xsl:value-of select="CategoryID"/>
                    </td>
                     <td>
                        <xsl:value-of select="QuantityPerUnit"/>
                    </td>
                     <td>
                        <xsl:value-of select="UnitsPrice"/>
                    </td>
                     <td>
                        <xsl:value-of select="UnitsInStock"/>
                    </td>
                     <td>
                        <xsl:value-of select="UnitsOnOrder"/>
                    </td>
                     <td>
                        <xsl:value-of select="ReorderLevel"/>
                    </td>
                     <td>
                        <xsl:value-of select="Discontinued"/>
                    </td>
                    </xsl:for-each>
            </table>
            <body>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
